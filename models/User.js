//[SECTION] Dependencies and Modules
const mongoose = require('mongoose'); 

//[SECTION] Blueprint Schema
    const userSchema = new mongoose.Schema({
    	firstName: {
			type: String,
			required: true,
		},

        lastName: {
            type: String,
            required: true,

        },
    
		email: {
			type: String,
			required: true,
			unique: true,
		},
		gender:{
			type: String,
			required: true,
		},
		password: {
			type: String,
			required: true,
		},
		isAdmin: {
			type: Boolean,
			required: true,
			default: false,
		},
	},
	{
		timestamps: true,
	
    });


//[SECTION] Model 
  const User = mongoose.model('User', userSchema)
  module.exports = User;
