//[SECTION] Dependencies and Modules
const exp = require("express"); 
const controller = require('./../controllers/user.js');
const auth = require('../auth')

// destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router(); 

//[SECTION] Routes-[POST]

//===REGISTER===//
  route.post('/register', controller.registerUser);
  
//===LOGIN===//
route.post('/login', (req, res) => {
  let userDetails = req.body;

  controller.loginUser(userDetails)
  .then(outcome=>{
    res.send(outcome);
  });

});



// route.post('/enroll', verify, controller.enroll);

// [SECTION] Routes-[GET]

//===USERSDETAILS===//
  route.get("/getUsers", verify, verifyAdmin, controller.getUsers)

//===USERDETAILS===//
  route.get("/getUserById", verify, verifyAdmin, controller.getUserById)


//[SECTION] Routes-[PUT]

//===UPDATEUSER===//

  route.put("/updateUser", verify, verifyAdmin, controller.updateUser)
//[SECTION] Routes-[DELETE]

//[SECTION] Expose Route System
  module.exports = route; 

