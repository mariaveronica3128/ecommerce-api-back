// // [SECTION]: Dependencies and Modules
// const exp = require("express");
// const controller = require('../controllers/orders.js');
// const User = require('../models/User');
// const auth = require("../auth");

// const {verify, verifyAdmin} = auth;

// // [SECTION]: Routing Component
// const route = exp.Router(); 

// // [SECTION]: POST ROUTES (Create)
// //authenticated user add to cart 
//   route.post('/add-to-cart', verify, (req,res)=>{
      
//       let orderDetails = req.body;
//       let cID = req.user.id;

//       controller.addToCart(cID,orderDetails).then(outcome => {
//         res.send(outcome);
//       });
//   })
  
//   route.post('/add-to-cart2', verify, (req,res)=>{
      
//       let orderDetails = req.body;
//       let cID = req.user.id;

//       controller.addtoCartAsyncAwait(cID,orderDetails).then(outcome => {
//         res.send(outcome);
//       });
//   })
// // guest checkout
//   route.post('/checkoutGuest', (req,res)=>{
      
//       let orderDetails = req.body;

//       controller.checkoutGuest(orderDetails).then(outcome => {
//         res.send(outcome);
//       });
//   })

// // [SECTION]: GET ROUTES (Retrieve)
// // Retrieve all orders
//   route.get('/all-orders', verify, verifyAdmin, (req, res) => {
   
//       controller.getAllOrders().then(result => {
//         res.send(result);
//       });
//    });
// // Retrieve orders from authenticated users
//   route.get('/all-auth-orders', verify, (req, res) => {
      
//       let id = req.user.id;
      
//       controller.getAuthenticatedOrders(id).then(result => {
//         res.send(result);
//       });
//    });

// // [SECTION]: PUT ROUTES (Update)
// // authenticated user's checkout
//   route.put('/userCheckout', verify, (req,res)=>{
      
//       let orderDetails = req.body;
//       let cID = req.user.id;

//       controller.authCheckout(cID, orderDetails).then(outcome => {
//         res.send(outcome);
//       });

//   })

// // [SECTION]: DELETE ROUTES (Delete)

// // [SECTION]: Export Route System
// module.exports = route;
