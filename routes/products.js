//[SECTION] Dependencies and Modules
const exp = require("express"); 
const controller = require('./../controllers/product.js');
const auth = require('../auth')

// destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
  const route = exp.Router(); 


// [SECTION] Routes-[POST]

//===CREATE PRODUCT===//

    route.post("/create", verify, verifyAdmin, controller.createProduct)

// [SECTION] Routes-[GET]

//===ProductsDETAILS===//
route.get("/items", verify, verifyAdmin, controller.getItems)

//===ProductDETAILS===//
  route.get("/item", verify, controller.getItem)

//===ProductsDETAILS(Active)===//
route.get("/aitems", verify, controller.getAItems)

//===ProductActivateorDeactivate===//

route.put('/archive', verify, verifyAdmin, (req,res) => {
  let productId = req.body.id;
  //console.log(courseId);

  controller.deactivateProduct(productId).then(result => {
      res.send(result);
  });
});


route.put('/reactivate',verify, verifyAdmin, (req,res) => {
  let productId = req.body.id;

  controller.reactivateProduct(productId).then(result => {
      res.send(result);
  });
});


//===UpdatePRODUCT===//
route.put('/:id',verify, verifyAdmin, (req, res) => {
      
  let id = req.params.id;
   let details = req.body;

   controller.updateProduct(id, details).then(result=>{
     res.send(result);
   })
 })



  //[SECTION] Expose Route System
  module.exports = route; 