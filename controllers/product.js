//[SECTION] Dependencies and Modules
const Product = require('../models/Products');

const auth = require('../auth') 

const asyncHandler = require ('express-async-handler')

const dotenv = require("dotenv");


//[SECTION] Environment Setup
dotenv.config();


//[SECTION] Functionalities [CREATE]

//=====CREATEAPRODUCT=====//
module.exports.createProduct = asyncHandler(async (req, res) => {
	const {
		name,
		price,
		description,
		category,
		countInStock,
	} = req.body

	const productExists = await Product.findOne({ name });
	if (productExists) {
		
		res.send({message:'Product Exists'})
	};

	const product = await Product.create({
		name,
		price,
		description,
		category,
		countInStock
	});

	if (product) {
        	res.send({
				_id: product._id,
                name:product.name,
				price: product.price,
				description: product.description,
				category:product.category,
				countInStock: product.countInStock,
				isActive:product.isActive
            })
       
	} else {
		res.send({message:'Invalid Product'})
	}
});

//[SECTION] Functionalities [RETRIEVE]


//=====RETRIEVING ALL ITEMS(ADMIN)=====//
module.exports.getItems = asyncHandler(async (req, res) => {
	const product = await Product.find()
	res.json(product)
});

//=====RETRIEVING ALL ITEMS(USER)=====//
module.exports.getAItems = asyncHandler(async (req, res) => {
	const product = await Product.find({isActive:true})
	res.json(product)
});

//=====RETRIEVING SINGLE PRODUCT=====//
module.exports.getItem = asyncHandler(async (req, res) => {
	const product = await Product.findById(req.body.id)
  console.log(product)
	if (product) {
		res.send(product)
	} else {
		res.status(404)
		res.send({message: 'Product not found'})
	}
});

// [SECTION]: FUNCTIONALITY - UPDATE
//Archive A Product
module.exports.deactivateProduct = (id) =>{
    let updates = {
    isActive: false
    }
    return Product.findByIdAndUpdate(id,updates).then((archived, error) =>{
        console.log(archived)
        if (archived ===null){
         return 'Cannot find product';
        }
        else if (!archived.isActive){
            return 'Already archived';
        }
        else {
           return 'Successfully archived product';
        }
     })
 };

 //Reactivate A Product
 module.exports.reactivateProduct = (id) =>{
    let updates = {
          isActive: true
    }
    return Product.findByIdAndUpdate(id,updates).then((reactivate, error) => {
       if (reactivate === null) {
        return 'Cannot activate Product'
       }
       else if (reactivate.isActive){
        return 'Product has been activated already'
        }
       else {
        return `Sucessfully activated product`
       }
    });
 };
 module.exports.updateProduct = (id, details) =>{
			
	let pName = details.name;
	let description = details.description;
	let price = details.price;
  	let category = details.category;
	let countInStock = details.countInStock;
	
  let updatedProduct = {
	name: pName,
	description: description,
	price: price,
	category: category,
	countInStock: countInStock
  }

  return Product.findByIdAndUpdate(id, updatedProduct).then((prodUpdated, err) => {
	
	if (err) {
	  return false;
	} else {
	  return  true;
	}

});
}  
