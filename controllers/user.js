//[SECTION] Dependencies and Modules
const User = require('../models/User');

const auth = require('../auth') 

const asyncHandler = require ('express-async-handler')

const bcrypt = require("bcrypt");
const dotenv = require("dotenv");
// const req = require('express/lib/request');



//[SECTION] Environment Setup
dotenv.config();
const salt = parseInt(process.env.SALT);


//[SECTION] Functionalities [CREATE]

//=====REGISTER=====//

module.exports.registerUser = asyncHandler(async (req, res) => {
	
	
	const { firstName,
		lastName,
        email,
		password,
        gender,
        mobileNum } = req.body;

	if(!firstName || !lastName || !password || !gender || !mobileNum || !email || !password) {
		res.send ({message: "Please complete all fields"})
	}

	const userExists = await User.findOne({ email });

	const hashedPassword = await bcrypt.hashSync(password, salt);

	if (userExists) {
		
		res.send(false)
	};

	const user = await User.create({
		firstName,
		lastName,
        email,
		password: hashedPassword,
        gender,
        mobileNum
	});

	if (user) {
        	res.send({
                _id: user._id,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                password: user.password,
                gender: user.gender,
                mobileNum: user.mobileNum,
                isAdmin: user.isAdmin,
            })
       
	} else {
		res.send({message:'Invalid User'})
	}
});

//=====LOGIN=====//
module.exports.loginUser = (userData) => {
			
	let email=userData.email;
	let passW=userData.password;

	return User.findOne({email:email}).then(result => {
		
		if (result === null){
			return false 
			// {message:('Email not found')}				
		// } else if (result.isActive === false){
		// 	return {message:('Account inactive. Request activate first')}
		}else {
			const isPasswordCorrect = bcrypt.compareSync(passW, result.password)
		 
		  if(isPasswordCorrect) {
			  return {accessToken: auth.createAccessToken(result), email: email, isAdmin: result.isAdmin}
		   } else {
			  return false
			  // {message: ("Incorrect Password")}
		   }
		}

	})
} 


   
//[SECTION] Functionalities [RETRIEVE]


//=====RETRIEVING ALL REGISTERED USER=====//
module.exports.getUsers = asyncHandler(async (req, res) => {
	const users = await User.find()
	res.json(users)
});

//=====RETRIEVING SINGLE USER=====//
module.exports.getUserById = asyncHandler(async (req, res) => {
	const user = await User.findById(req.body.id)
  console.log(user)
	if (user) {
		res.json(user)
	} else {
		res.status(404)
		throw new Error('User not found')
	}
});

//[SECTION] Functionalities [UPDATE]

//=====UPDATE USER=====/
module.exports.updateUser = asyncHandler(async (req, res) => {
	const user = await User.findById(req.body.id)

	if (user) {
		user.firstName = req.body.firstName || user.firstName
    user.lastName = req.body.lastName || user.lastName
		user.email = req.body.email || user.email
    user.gender = req.body.gender || user.gender
    user.mobileNum = req.body.mobileNum || user.mobileNum
	

		const updatedUser = await user.save()

		res.json({
			_id: updatedUser._id,
			fistName: updatedUser.name,
			lastName: updatedUser.name,
			email: updatedUser.email,
			gender: updatedUser.gender,
			mobileNum: updatedUser.mobileNum,
		
		})

	} else {
		res.send({message: "User not found"})
	}
})


//[SECTION] Functionalities [DELETE]
